function c = speed_of_sound( u, gamma )

[rho,~,p] = get_primitive(u,gamma);
c = sqrt(gamma*p/rho);

end

