function unew = forward_euler( uold, dt, dx, gamma, ng, npts, space_method )

rhs = compute_rhs(uold, dx, gamma, ng, npts, space_method);

%% compute new solution
unew = uold;
for i=(ng+1):(npts-ng)
    unew(i,:) = unew(i,:) + dt*rhs(i, :);
end
unew = apply_bc(unew, gamma, ng, npts);

end

