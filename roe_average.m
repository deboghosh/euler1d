function uavg = roe_average( u1, u2, gamma )

uavg = zeros(1,3);

[rho1,v1,~] = get_primitive(u1, gamma);
c1 = speed_of_sound(u1, gamma);
H1 = 0.5*v1*v1 + c1*c1/(gamma-1);

[rho2,v2,~] = get_primitive(u2,gamma);
c2 = speed_of_sound(u2, gamma);
H2 = 0.5*v2*v2 + c2*c2/(gamma-1);

t1 = sqrt(rho1);
t2 = sqrt(rho2);

rho = t1*t2;
v = (t1*v1 + t2*v2) / (t1+t2);
H = (t1*H1 + t2*H2) / (t1+t2);
csq = (gamma-1) * (H - 0.5*v*v);
p = csq * rho / gamma;
E = p/(gamma-1) + 0.5*rho*v*v;

uavg(1) = rho;
uavg(2) = rho*v;
uavg(3) = E;

end

