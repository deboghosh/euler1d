function plot_solution( u, x, figrho, figv, figp, npts, ng, gamma, dx, style )

x_wg = zeros(npts,1);
x_wg((ng+1):(npts-ng)) = x;
for i = ng:-1:1
    x_wg(i) = x_wg(i+1) - dx;
end
for i = (npts-ng+1):npts
    x_wg(i) = x_wg(i-1) + dx;
end


q = zeros(size(u,1),size(u,2));
for i = 1:npts
    q(i,:) = get_primitive(u(i,:),gamma);
end

figure(figrho);
plot(x_wg, q(1), style, 'Linewidth',2);
hold on;
figure(figv);
plot(x_wg, q(2), style, 'Linewidth',2);
hold on;
figure(figp);
plot(x_wg, q(3), style, 'Linewidth',2);
hold on;

end

