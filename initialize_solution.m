function u = initialize_solution( npts, ng, x, gamma, delta, ptype )

% Initialize the solution with a constant slab
% on the left side of the domain

u = zeros(npts,3);

for i=(ng+1):(npts-ng)
    if (strcmp(ptype,'sod'))
        if (x(i-ng) < 0.5)
            rho = 1.0;
            v = 0.0;
            p = 1.0;
        else
            rho = 0.125;
            v = 0.0;
            p = 0.1;
        end
    else
        if (x(i-ng) <= delta)
            rho = 1.0;
            v = 0.0;
            p = 1.0;
        else
            rho = 1e-6;
            v = 0.0;
            p = 1e-6;
        end
    end
    u(i,:) = get_conserved(rho,v,p,gamma);
end

end

