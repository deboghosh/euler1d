clear all;
close all;

%% set the simulation parameters
gamma = 1.4;        % specific heat ratio

npts = 201;         % number of grid points
ng = 3;             % number of ghost points

L = 1.0;            % domain length
delta = 0.1;        % slab width

mode = 'fixed_cfl';
cfl = 0.5;          % CFL (relevant if mode is fixed_cfl)
dt = 0.0002;        % time step size (relevant if mode is fixed_dt)

tfinal = 0.25;     % final time
maxits = 1000000;   % max iterations

plot_int = 0.05;     % plot interval
screen_int = 0.05;    % screen output interval

ti_method = 'rk4';
space_method = 'weno5';

ptype = 'sod';

npts_total = npts + 2*ng;

%% open figure windows
% open figure windows
scrsz = get(0,'ScreenSize');
figrho = figure('Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2]);
figv   = figure('Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2]);
figp   = figure('Position',[1 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2]);

%% create grid
dx = L/(npts-1);
x = zeros(npts,1);
for i = 1:npts
    x(i) = (i-1)*dx;
end

x_wg = zeros(npts_total,1);
x_wg((ng+1):(npts_total-ng)) = x;
for i = ng:-1:1
    x_wg(i) = x_wg(i+1) - dx;
end
for i = (npts_total-ng+1):npts_total
    x_wg(i) = x_wg(i-1) + dx;
end

%% initialize
u = initialize_solution(npts_total,ng,x,gamma,delta,ptype);
u = apply_bc( u, gamma, ng, npts_total );

%% plot initial solution
q = cons_to_prim(u, npts_total, gamma);

rho_max = max(q(:,1));
rho_min = min(q(:,1));
v_max = max(q(:,2));
v_min = min(q(:,2));
p_max = max(q(:,3));
p_min = min(q(:,3));

style = '-k';
figure(figrho);
plot(x_wg, q(:,1), style, 'Linewidth',2);
hold on;
figure(figv);
plot(x_wg, q(:,2), style, 'Linewidth',2);
hold on;
figure(figp);
plot(x_wg, q(:,3), style, 'Linewidth',2);
hold on;

figure(figrho);
xlabel('x','FontName','Times','FontSize',20,'FontWeight','normal');
ylabel('Density','FontName','Times','FontSize',20,'FontWeight','normal');
set(gca,'FontSize',14,'FontName','Times');
axis([min(x_wg) max(x_wg) 0.9*rho_min 1.1*rho_max]);
grid on;
hold on;
figure(figv);
xlabel('x','FontName','Times','FontSize',20,'FontWeight','normal');
ylabel('Velocity','FontName','Times','FontSize',20,'FontWeight','normal');
set(gca,'FontSize',14,'FontName','Times');
axis([min(x_wg) max(x_wg) (v_min-0.01) (v_max+0.01)]);
grid on;
hold on;
figure(figp);
xlabel('x','FontName','Times','FontSize',20,'FontWeight','normal');
ylabel('Pressure','FontName','Times','FontSize',20,'FontWeight','normal');
set(gca,'FontSize',14,'FontName','Times');
axis([min(x_wg) max(x_wg) 0.9*p_min 1.1*p_max]);
grid on;
hold on;

%% time loop
cur_time = 0.0;
iter = 1;
t_plot = 0.0;
t_screen = 0.0;
while ((cur_time < tfinal) && (iter <= maxits))
    
    %% compute time step
    if (strcmp(mode,'fixed_cfl'))
        dt = compute_dt(u, dx, npts_total, ng, cfl, gamma);
    else
        cfl = compute_cfl(u, dx, npts_total, ng, dt, gamma);
    end
    if ((cur_time+dt) > tfinal)
        dt = tfinal - cur_time;
        final_iter = 1;
    else
        final_iter = 0;
    end
    if (iter == maxits)
        final_iter = 1;
    end
    
    %% take a step
    if (strcmp(ti_method,'rk4'))
        unew = rk4(u, dt, dx, gamma, ng, npts_total, space_method);
    else
        unew = forward_euler(u, dt, dx, gamma, ng, npts_total, space_method);    
    end
    
    %% print stuff to screen
    if ((t_screen >= screen_int) || (final_iter == 1))
        fprintf('iter=%5d, dt=%1.4e, cfl=%1.4e, cur_time=%3.4f, norm=%1.4e\n', ...
                iter, dt, cfl, cur_time+dt, norm(unew-u,2));
        t_screen = 0;
    else
       t_screen = t_screen + dt; 
    end
    
    %% update solution, current time and iteration counter
    u = unew;
    cur_time = cur_time + dt;
    iter = iter+1;
    
    %% plot solution
    if ((t_plot >= plot_int) || (final_iter == 1))
        q = cons_to_prim(u, npts_total, gamma);
        
        rho_max = max(rho_max,max(q(:,1)));
        rho_min = min(rho_min,min(q(:,1)));
        v_max = max(v_max,max(q(:,2)));
        v_min = min(v_min,min(q(:,2)));
        p_max = max(p_max,max(q(:,3)));
        p_min = min(p_min,min(q(:,3)));
        
        if (final_iter == 1)
            style = '-b';
        else
            style = ':r';
        end
        figure(figrho);
        plot(x_wg, q(:,1), style, 'Linewidth',2);
        axis([min(x_wg) max(x_wg) 0.9*rho_min 1.1*rho_max]);
        hold on;
        figure(figv);
        plot(x_wg, q(:,2), style, 'Linewidth',2);
        axis([min(x_wg) max(x_wg) v_min v_max]);
        hold on;
        figure(figp);
        plot(x_wg, q(:,3), style, 'Linewidth',2);
        axis([min(x_wg) max(x_wg) 0.9*p_min 1.1*p_max]);
        hold on;    
        
        t_plot = 0;
    else
        t_plot = t_plot + dt;
    end
    
end

%% write solution to file
fid = fopen('op.dat','w');
for i = 1:npts
    fprintf(fid,'%3d  %+1.16e  %+1.16e  %+1.16e  %+1.16e\n', ...
            i-1, x(i), u(i+ng,1), u(i+ng,2), u(i+ng,3));
end
fclose(fid);


%% wrap up figures
figure(figrho);
hold off;
figure(figv);
hold off;
figure(figp);
hold off;