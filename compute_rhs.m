function rhs = compute_rhs( u, dx, gamma, ng, npts, method )

%% apply boundary conditions
u = apply_bc(u, gamma, ng, npts);

%% compute flux at cell centers
flux = compute_flux(u, gamma, npts);

%% reconstruct flux at interfaces
npts_int = npts - 2*ng;
f_int = reconstruct(flux, u, npts_int, ng, gamma, method);

%% compute RHS
rhs = zeros(size(u,1),size(u,2));
for i=1:(npts-2*ng)
    rhs(i+ng,:) = (1.0/dx)*(f_int(i+1,:)-f_int(i,:));
end

end

