function u_weno = compute_weno_interp( u )

um2 = u(1,:);
um1 = u(2,:);
uc  = u(3,:);
up1 = u(4,:);
up2 = u(5,:);

beta = zeros(3,size(u,2));
beta(1,:) = (13.0/12.0)*(um2-2*um1+uc).^2+(1.0/4.0)*(um2-4*um1+3*uc).^2;
beta(2,:) = (13.0/12.0)*(um1-2*uc+up1).^2+(1.0/4.0)*(um1-up1).^2;
beta(3,:) = (13.0/12.0)*(uc-2*up1+up2).^2+(1.0/4.0)*(3*uc-4*up1+up2).^2;

eps = 1e-6;
c = [0.1; 0.6; 0.3];

alpha = zeros(3,size(u,2));
for i = 1:3
    alpha(i,:) = c(i) ./ ((eps + beta(i,:)).^2);
end

alpha_sum = zeros(1,size(u,2));
for i = 1:3
    for j = 1:size(u,2)
        alpha_sum(j) = alpha_sum(j) + alpha(i,j);
    end
end

omega = zeros(3, size(u,2));
for i = 1:3
    for j = 1:size(u,2)
        omega(i,j) = alpha(i,j) ./ alpha_sum(j);
    end
end

w = zeros(3,size(u,2));
w(1,:) = um2(:)/3.0 - (7.0/6.0)*um1(:) + (11.0/6.0)*uc(:);
w(2,:) = -um1(:)/6.0 + (5.0/6.0)*uc(:) + up1(:)/3.0;
w(3,:) = uc(:)/3.0 + (5.0/6.0)*up1(:) - up2(:)/6.0;

u_weno = zeros(1,size(u,2));
u_weno(:) = omega(1,:).*w(1,:) + omega(2,:).*w(2,:) + omega(3,:).*w(3,:);

end

