function q = cons_to_prim( u, npts, gamma )

q = zeros(size(u,1),size(u,2));
for i = 1:npts
    [q(i,1),q(i,2),q(i,3)] = get_primitive(u(i,:),gamma);
end


end

