function u = get_conserved( rho, v, p, gamma )

u = zeros(1,3);
u(1) = rho;
u(2) = rho*v;
u(3) = p/(gamma-1) + 0.5*rho*v*v;

end

