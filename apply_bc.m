function u = apply_bc( u_in, gamma, ng, npts )

u = u_in;

%% left boundary
for i = 1:ng
    j = 2*ng - (i-1);
    [rho,v,p] = get_primitive(u(j,:),gamma);
    rho_gpt = rho;
    p_gpt = p;
    v_gpt = -v;
    u(i,:) = get_conserved(rho_gpt, v_gpt, p_gpt, gamma);
end

%% right boundary
for i = (npts-ng+1):npts
    j = (npts-ng) - (i-(npts-ng+1));
    [rho,v,p] = get_primitive(u(j,:),gamma);
    rho_gpt = rho;
    p_gpt = p;
    v_gpt = -v;
    u(i,:) = get_conserved(rho_gpt, v_gpt, p_gpt, gamma);
end

end

