function [ rho, v, p ] = get_primitive( u, gamma )

rho = u(1);
v = u(2)/rho;
p = (gamma-1)*(u(3) - 0.5*rho*v*v);

end

