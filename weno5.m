function u_int = weno5( u, npts, ng, bias )

npts_int = npts+1;

u_int = zeros(npts_int,3);
if (bias == 1)
    for i = 1:npts_int
        stencil = zeros(5,size(u,2));
        stencil(1,:) = u(i+ng-3,:);
        stencil(2,:) = u(i+ng-2,:);
        stencil(3,:) = u(i+ng-1,:);
        stencil(4,:) = u(i+ng  ,:);
        stencil(5,:) = u(i+ng+1,:);
        u_int(i,:) = compute_weno_interp(stencil);
    end
else
    for i = 1:npts_int
        stencil = zeros(5,size(u,2));
        stencil(1,:) = u(i+ng+2,:);
        stencil(2,:) = u(i+ng+1,:);
        stencil(3,:) = u(i+ng  ,:);
        stencil(4,:) = u(i+ng-1,:);
        stencil(5,:) = u(i+ng-2,:);
        u_int(i,:) = compute_weno_interp(stencil);
    end
end

end
