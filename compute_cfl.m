function cfl = compute_cfl( u, dx, npts, ng, dt, gamma )

max_wavespeed = 0;

for i = (ng+1):(npts-ng)
    [~,v,~] = get_primitive(u(i,:), gamma);
    c = speed_of_sound(u(i,:), gamma);
    if ((abs(v)+c) > max_wavespeed) 
        max_wavespeed = abs(v)+c;
    end
end

cfl = max_wavespeed * dt / dx;

end