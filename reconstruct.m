function f_int = reconstruct( flux, u, npts, ng, gamma, method )

npts_int = npts+1;

%% compute flux and solution at interface
if(strcmp(method, '1'))
    uL = first_order_upwind( u, npts, ng,  1);
    uR = first_order_upwind( u, npts, ng, -1);
    fL = first_order_upwind( flux, npts, ng,  1);
    fR = first_order_upwind( flux, npts, ng, -1);
elseif (strcmp(method,'weno5'))
    uL = weno5( u, npts, ng,  1);
    uR = weno5( u, npts, ng, -1);
    fL = weno5( flux, npts, ng,  1);
    fR = weno5( flux, npts, ng, -1);
end

%% compute upwind flux (Rusanov)
f_int = zeros(npts_int,3);
for i = 1:npts_int
    
    uavg = roe_average(u(i+ng-1,:), u(i+ng,:), gamma);
    
    [~,v,~] = get_primitive(u(i+ng-1,:),gamma);
    c = speed_of_sound(u(i+ng-1,:),gamma);
    alphaL = abs(v)+c;
    
    [~,v,~] = get_primitive(u(i+ng,:),gamma);
    c = speed_of_sound(u(i+ng,:),gamma);
    alphaR = abs(v)+c;
    
    [~,v,~] = get_primitive(uavg,gamma);
    c = speed_of_sound(uavg,gamma);
    alpha_avg = abs(v)+c;
    
    alpha = max(alpha_avg,max(alphaL,alphaR));
    
    f_int(i,:) = 0.5 * ( (fL(i,:)+fR(i,:)) + alpha*(uR(i,:)-uL(i,:)) );
end

end

