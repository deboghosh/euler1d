function flux = compute_flux( u, gamma, npts )

flux = zeros(npts,3);
for i=1:npts
    [rho,v,p] = get_primitive(u(i,:),gamma);
    flux(i,1) = rho*v;
    flux(i,2) = rho*v*v + p;
    flux(i,3) = (u(i,3)+p)*v;
end

end

