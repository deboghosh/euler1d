function unew = rk4( uold, dt, dx, gamma, ng, npts, space_method )

u1 = uold;
f1 = compute_rhs(u1, dx, gamma, ng, npts, space_method);

u2 = uold + 0.5*dt*f1;
f2 = compute_rhs(u2, dx, gamma, ng, npts, space_method);

u3 = uold + 0.5*dt*f2;
f3 = compute_rhs(u3, dx, gamma, ng, npts, space_method);

u4 = uold + dt*f3;
f4 = compute_rhs(u4, dx, gamma, ng, npts, space_method);

unew = uold + (dt/6.0)*f1 + (dt/3.0)*f2 + (dt/3.0)*f3 + (dt/6.0)*f4;
unew = apply_bc(unew, gamma, ng, npts);

end

