function u_int = first_order_upwind( u, npts, ng, bias )

npts_int = npts+1;

u_int = zeros(npts_int,3);
if (bias == 1)
    for i = 1:npts_int
        u_int(i,:) = u(i+ng-1,:);
    end
else
    for i = 1:npts_int
        u_int(i,:) = u(i+ng,:);
    end
end

end

