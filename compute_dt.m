function dt = compute_dt( u, dx, npts, ng, cfl, gamma )

max_wavespeed = 0;

for i = (ng+1):(npts-ng)
    [~,v,~] = get_primitive(u(i,:), gamma);
    c = speed_of_sound(u(i,:), gamma);
    if ((abs(v)+c) > max_wavespeed) 
        max_wavespeed = abs(v)+c;
    end
end

dt = cfl * dx / max_wavespeed;

end

